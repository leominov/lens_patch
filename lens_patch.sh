#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

lens_dir=${LENS_DIR:-/Applications/Lens.app/Contents/Resources}

if ! asar --help >/dev/null 2>&1; then
  echo 'asar binary not found. Try: npm install --global @electron/asar'
  exit 1
fi

if ! cd "${lens_dir}"; then
  echo 'No Lens directory found'
  exit 1
fi

if [ -f 'app.asar.backup' ]; then
  echo 'Found backup file, Lens already patched?'
  exit 1
fi

echo 'Creating backup file...'
if ! cp app.asar app.asar.backup; then
  echo 'Failed to create backup file'
  exit 1
fi

echo 'Extracting asar file...'
if ! asar extract app.asar _tmp; then
  echo 'Failed to extract asar file'
  exit 1
fi

# Remove temporary directory after all
trap 'rm -rf ${lens_dir}/_tmp' EXIT

echo 'Injecting code...'
echo '<style>#portal-wrapper{display:none!important;};</style>' >> _tmp/static/build/index.html

echo 'Creating asar file...'
if ! asar pack _tmp app.asar; then
  echo 'Failed to create asar file'
  exit 1
fi

echo 'Done'
