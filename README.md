# lens_patch

For informational purposes only.

## Supported platforms

* macOS

## Software requirements

* [asar](https://github.com/electron/asar)

## Usage

```shell
./lens_patch.sh
```
